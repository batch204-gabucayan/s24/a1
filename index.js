// alert('YOU ARE IN MASTER!')

// exponent operator
let getCube = 5 ** 3;
console.log(`The cube of 5 is ${getCube}`); // template literal is ${}

// address array
const address = ['Matahimik', 'Tiniguiban', 'Puerto Princesa City'];

// Destructure Array with Template Literals
const [streetAdd, brgyAdd, cityAdd] = address;
console.log(`I live at ${streetAdd} street, Brgy. ${brgyAdd}, ${cityAdd}.`);


// animal Object
const animal = {
	type: 'saltwater crocodile',
	weight: 1075,
	length: '20 ft 3 in',
	name: 'Lolong'
};

// Destructure Object with Template literals
const{type, weight, length, name} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length}.
	`);

// number array
const arrayofNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9 , 10];

// Looping using .forEach() with arrow function and implicit return statement
arrayofNumbers.forEach((number) => console.log(number));
// class Dog with constructor 
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

// Creating/Instantiating a new object 
const dogOne = new Dog('Timone', 2, 'Great Dane');
console.log(dogOne); // printing the new object in console